## Avaliação para desenvolvedor pleno


### Informações importantes:

- Data ideal de entrega: **1 semana a partir do recebimento**;
- O exercício é de execução individual;
- O teste deve ser feito em NodeJS;
- A solução do exercício deverá ser encaminhada para a Kognita em um repositório gratuito (ex: GitHub, GitLab, Bitbucket) e documentado;
- Os passos necessários para execução do projeto devem estar descritos em um arquivo README;
- O teste não tem caráter eliminatório, apenas classificatório. Portanto, mesmo caso não consiga resolvê-lo por completo, envie a solução até onde conseguiu avançar;
- Qualquer dúvida, não hesite em entrar em contato.

### Exercício:

#### Cenário: 
O teste consiste em desenvolver uma API REST para listar, adicionar e excluir informações de uma lista pessoas jurídicas com os seguintes dados: razão social, nome fantasia (opcional), CNPJ e data de abertura.

#### Requisitos: 

- TypeScript, TypeORM e Express;
- A persistência das informações deve ser em base de dados relacional (desejável MySQL);
- Não é necessário editar informações. 



#### Diferenciais
- Ambiente de desenvolvimento com Docker;
- Padrões de desenvolvimento SOLID.

